# Timer App

A simple timer app to test how Stateful Widgets work including the Timer class

![Timer App](docs/timer-app-1.webm){width=800 height=600}