import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const TimerApp());
}

class TimerApp extends StatelessWidget {
  const TimerApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        home: Scaffold(
            appBar:
                AppBar(leading: Icon(Icons.timer), title: const Text('Timer')),
            body: Center(
              child: TimerWidget(),
            )));
  }
}

class TimerWidget extends StatefulWidget {
  const TimerWidget({super.key});

  @override
  State<TimerWidget> createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> {
  var now = DateTime.now();
  var isActive = true;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), _updateTime);
    setState(() {
      isActive = true;
    });
  }

  void _stopTimer() {
    _timer?.cancel();

    setState(() {
      isActive = false;
    });
  }

  void _updateTime(Timer timer) {
    setState(() {
      now = DateTime.now();
    });
  }

  void _toggleTimer() {
    if (isActive) {
      _stopTimer();
    } else {
      _startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    var formattedNowDate = DateFormat("EEE, d MMM yyyy").format(now);
    var formattedNowTime = DateFormat("HH:mm:ss").format(now);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(formattedNowDate,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        const SizedBox(height: 10),
        Text(formattedNowTime,
            style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
                color: isActive ? Colors.green : Colors.red)),
        ElevatedButton(
            onPressed: isActive ? _stopTimer : _startTimer,
            child: Text('${isActive ? 'Stop' : 'Start'} Timer'))
      ],
    );
  }
}
